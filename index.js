const form=document.getElementById("form");
const username=document.getElementById("username");
const email=document.getElementById("email");
const password=document.getElementById("password");
const confirmpassword=document.getElementById("confirmpassword");


// function you(){
//     alert("clicked");
// }

form.addEventListener('submit',e=>{
    // alert("hi")
    e.preventDefault();
    checkInput();
});

function checkInput(){
    // alert('0');
    const usernameValue= username.value.trim();
    const emailValue= email.value.trim();
    const passwordValue= password.value.trim();
    const confirmPasswordValue=  confirmpassword.value.trim();
    // alert('10');
    
    if(usernameValue===''){
        setError(username,'Username cannot be blank');
                // return true;

    } else{
        setSuccess(username);
        // return false;
    }


    if(emailValue===''){

        setError(email,'email cannot be blank');
        // return false;

    }
    else if(!isEmail(emailValue)){

        setError(email,'email is not valid');
    }
    else{
        setSuccess(email);

    }

    if(passwordValue===''){
        setError(password,'Password cannot be blank');
    } else{
        setSuccess(password);
    }
    if(confirmPasswordValue===''){
        // alert('1');

        setError(confirmpassword,'Current password cannot be blank');
    }else if(confirmPasswordValue !== password){
        // alert('2');
        setError(confirmpassword,'Current password does not match');
    }
    
    else{
        // alert('3');

        setSuccess(confirmpassword);
    }




}
function setError(input,message){
    console.log("error");
    const formControl=input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className='form-control error';
    small.innerText=message;
}


function setSuccess(input){
    console.log("no error");

    const formControl=input.parentElement;
    formControl.className='form-control success';
    
}


function isEmail(email){
    return  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}